<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Search\Data;

use apathy\ThunderDome\Entity\Hive;
use XF\Mvc\Entity\Entity;
use XF\Search\IndexRecord;
use XF\Search\Data\AbstractData;
use XF\Search\MetadataStructure;

class Player extends AbstractData
{
    public function getIndexData(Entity $entity): IndexRecord
    {
        $index = IndexRecord::create('td_player', $entity->steam_id, [
            'title'    => $entity->name,
            'metadata' => []
        ]);

        return $index;
    }

    protected function getMetaData(Hive $entity)
    {
        return false;
    }

    public function setupMetadataStructure(MetadataStructure $structure)
    {
        return false;
    }
        
    public function getResultDate(Entity $entity)
    {
        return false;
    }

    public function getTemplateData(Entity $entity, array $options = []): array
    {
        return [
            'player'  => [$entity],
            'options' => $options
        ];
    }
}

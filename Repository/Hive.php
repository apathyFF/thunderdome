<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Repository;

use apathy\ThunderDome\Entity\Hive as Player;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;
use XF;
use XF\Mvc\Entity\Repository;

class Hive extends Repository
{
    /** Used for excluding round results prior to this timestamp */
    const EXCLUDE_ROUND_TIMESTAMP = 1684429200;

    /** Used for converting Steam IDs */
    const STEAM_ID_MODIFIER = 76561197960265728;

    public function attemptConnection(string $api)
    {
        try
        {
            $contents = $this->app()->http()->client()->get($api);
            
            return GuzzleHttp\json_decode($contents->getBody(), true);
        }
        catch(RequestException $e)
        {
            if(null !== $e->getResponse())
            {
                $error = 'Error: ' . $e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase();
            }
            else
            {
                $error = $e->getMessage();
            }

            throw new XF\PrintableException($error);
        }
    }

    public function fetchApi(int $steamId)
    {
        /** https://steamcommunity.com/dev/apikey */
        $apiKey   = $this->app()->options()->apTdSteamApiKey;
        $endpoint = 'https://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v2';
        $steamId  = $this->convertId3To64($steamId);

        $api = $endpoint . "/?key={$apiKey}&steamid={$steamId}&appid=4920";

        return $this->attemptConnection($api);
    }

    /** @var $limitResults   Excludes data older than PIGGU's revival 18-May-2023 */
    public function findPlayersForList(bool $limitResults = false)
    {
        $finder = $this->finder('apathy\ThunderDome:Hive');
        
        if($limitResults)
        {
            $finder->where('last_fetch_date', '>=', $this::EXCLUDE_ROUND_TIMESTAMP);
        }

        return $finder;
    }

    /** @var $limitResults   Excludes data older than PIGGU's revival 18-May-2023 */
    public function findPlayerBySteamId(int $steamId, bool $limitResults = false)
    {
        $finder = $this->finder('apathy\ThunderDome:Hive')->where('steam_id', $steamId);
        
        if($limitResults)
        {
            $finder->where('last_fetch_date', '>=', $this::EXCLUDE_ROUND_TIMESTAMP);
        }

        return $finder->fetchOne();
    }

    /** @var $limitResults   Excludes data older than PIGGU's revival 18-May-2023 */
    public function findPlayerHistoryBySteamId(int $steamId, bool $limitResults = false)
    {
        $finder = $this->finder('apathy\ThunderDome:History')->with('Hive')->where('steam_id', $steamId);

        if($limitResults)
        {
            $finder->where('Hive.last_fetch_date', '>=', $this::EXCLUDE_ROUND_TIMESTAMP);
            $finder->where('date', '>=', $this::EXCLUDE_ROUND_TIMESTAMP);
        }

        return $finder;
    }

    public function rebuildPlayerData(Player $player, array $stats, int $time)
    {
        $db = $this->db();
		$db->beginTransaction();

        $skill = $stats['td_skill']['value'] ?? 0;
        
        $input = [
            'steam_id'                => $this->convertId64To3($stats['steamID']),
            'name'                    => $stats['name'],
            'td_skill'                => $skill,
            'td_skill_offset'         => $stats['td_skill_offset']['value']             ?? 0,
            'td_comm_skill'           => $stats['td_comm_skill']['value']               ?? 0,
            'td_comm_skill_offset'    => $stats['td_comm_skill_offset']['value']        ?? 0,
            'tier'                    => $player->findTier($skill),
            'td_total_time_player'    => $stats['td_total_time_player']['value']        ?? 0,
            'td_total_time_commander' => $stats['td_total_time_commander']['value']     ?? 0,
            'td_rounds_won_player'    => $stats['td_rounds_won_player']['value']        ?? 0,
            'td_rounds_won_commander' => $stats['td_rounds_won_commander']['value']     ?? 0,
            'last_fetch_date'         => $time
        ];

        $input['td_alien_skill']  = ( $input['td_skill'] - $input['td_skill_offset'] );
        $input['td_marine_skill'] = ( $input['td_skill'] + $input['td_skill_offset'] );

        $input['td_alien_comm_skill']  = ( $input['td_comm_skill'] - $input['td_comm_skill_offset'] );
        $input['td_marine_comm_skill'] = ( $input['td_comm_skill'] + $input['td_comm_skill_offset'] );

        $player->bulkSet($input);

        $player->save(true, false);

        /** Save it to the history */
        unset($input['last_fetch_date']);
        
        $history = $this->app()->em()->create('apathy\ThunderDome:History');

        $history->bulkSet($input);

        $history->save(true, false);

        $db->commit();
    }

    

    protected function convertId3To64(int $steamId): int
    {
        return ( $steamId + $this::STEAM_ID_MODIFIER );
    }

    protected function convertId64To3(int $steamId): int
    {
        return ( $steamId - $this::STEAM_ID_MODIFIER );
    }
}

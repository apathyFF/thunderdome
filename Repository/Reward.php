<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Repository;

use XF\Mvc\Entity\Repository;

class Reward extends Repository
{
    public function findWinRewards(): array
    {
        return [
            'skulk_huggies' => [
                'title' => 'Skulk huggies',
                'value' => 3
            ],
            'chroma_grenade_launcher' => [
                'title' => 'Chroma grenade launcher',
                'value' => 5
            ],
            'dont_blink' => [
                'title' => 'Don\'t blink!',
                'value' => 7
            ],
            'damascus_purple_rifle'   => [
                'title' => 'Damascus purple rifle',
                'value' => 10
            ],
            'baby_marine' => [
                'title' => 'Baby marine',
                'value' => 13
            ],
            'for_science' => [
                'title' => 'For science',
                'value' => 15
            ],
            'damascus_purple_axe' => [
                'title' => 'Damascus purple axe',
                'value' => 15
            ],
            'locked_and_loaded' => [
                'title' => 'Locked and loaded',
                'value' => 17
            ],
            'damascus_black_axe' => [
                'title' => 'Damascus black axe',
                'value' => 20
            ],
            'nerd_rage' => [
                'title' => 'Nerd rage',
                'value' => 23
            ],
            'damascus_purple_pistol'  => [
                'title' => 'Damascus purple pistol',
                'value' => 25
            ],
            'urpa_booty' => [
                'title' => 'Urpa booty',
                'value' => 27
            ],
            'chroma_hmg' => [
                'title' => 'Chroma HMG',
                'value' => 30
            ],
            'sad_babbler' => [
                'title' => 'Sad babbler',
                'value' => 33
            ],
            'a_job_weld_done' => [
                'title' => 'A job weld done',
                'value' => 37
            ],
            'auric_clog' => [
                'title' => 'Auric clog',
                'value' => 40
            ],
            'balance_gorge' => [
                'title' => 'Balance gorge',
                'value' => 43
            ],
            'damascus_black_pistol' => [
                'title' => 'Damascus black pistol',
                'value' => 45
            ],
            'l0rk' => [
                'title' => 'L0rk',
                'value' => 47
            ],
            'chroma_shotgun' => [
                'title' => 'Chroma shotgun',
                'value' => 50
            ],
            'lazy_gorge' => [
                'title' => 'Lazy gorge',
                'value' => 55
            ],
            'auric_gorge' => [
                'title' => 'Auric gorge',
                'value' => 65
            ],
            'skulk_slippers' => [
                'title' => 'Skulk slippers',
                'value' => 70
            ],
            'shadow_fade' => [
                'title' => 'Shadow fade',
                'value' => 75
            ],
            'auric_hydra' => [
                'title' => 'Auric hydra',
                'value' => 80
            ],
            'burnout_fade' => [
                'title' => 'Burnout fade',
                'value' => 85
            ],
            'over_9000_degrees' => [
                'title' => 'Over 9000 degrees',
                'value' => 90
            ],
            'damascus_black_rifle' => [
                'title' => 'Damascus black rifle',
                'value' => 100
            ],
            'lerked' => [
                'title' => 'Lerked',
                'value' => 200
            ],
            'gorge_table_flip' => [
                'title' => 'Gorge table flip',
                'value' => 250
            ],
            'auric_lerk' => [
                'title' => 'Auric lerk',
                'value' => 300
            ],
            'angry_onos' => [
                'title' => 'Angry onos',
                'value' => 350
            ],
            'oh_noes' => [
                'title' => 'Oh noes!',
                'value' => 400
            ],
            'auric_fade' => [
                'title' => 'Auric fade',
                'value' => 500
            ]
        ];
    }

    public function findCommWinRewards(): array
    {
        return [
            'auric_egg' => [
                'title' => 'Auric egg',
                'value' => 10
            ],
            'auric_tunnel' => [
                'title' => 'Auric tunnel',
                'value' => 20
            ],
            'turbo_drifter' => [
                'title' => 'Turbo drifter',
                'value' => 25
            ],
            'auric_hive' => [
                'title' => 'Auric hive',
                'value' => 45
            ],
            'battle_gorge' => [
                'title' => 'Battle gorge',
                'value' => 60
            ],
            'chroma_command_station' => [
                'title' => 'Chroma command station',
                'value' => 80
            ]
        ];
    }

    public function findCommTimeRewards(): array
    {
        return [
            'chroma_extractor' => [
                'title' => 'Chroma extractor',
                'value' => 10
            ],
            'chroma_arc' => [
                'title' => 'Chroma arc',
                'value' => 20
            ],
            'auric_cyst' => [
                'title' => 'Auric cyst',
                'value' => 45
            ],
            'auric_drifter' => [
                'title' => 'Auric drifters',
                'value' => 60
            ],
            'chroma_mac' => [
                'title' => 'Chroma mac',
                'value' => 90
            ]
        ];
    }

    public function findTimeRewards(): array
    {
        return [
            'wood_rifle' => [
                'title' => 'Wood rifle',
                'value' => 1
            ],
            'wood_pistol' => [
                'title' => 'Wood pistol',
                'value' => 2
            ],
            'wood_axe' => [
                'title' => 'Wood axe',
                'value' => 3
            ],
            'damascus_green_pistol' => [
                'title' => 'Damascus green pistol',
                'value' => 4
            ],
            'damascus_green_axe' => [
                'title' => 'Damascus green axe',
                'value' => 6
            ],
            'mad_axe_gorge' => [
                'title' => 'Mad axe gorge',
                'value' => 9
            ],
            'chroma_flamethrower' => [
                'title' => 'Chroma flamethrower',
                'value' => 12
            ],
            'auric_skulk' => [
                'title' => 'Auric skulk',
                'value' => 16
            ],
            'chroma_axe' => [
                'title' => 'Chroma axe',
                'value' => 26
            ],
            'auric_babbler' => [
                'title' => 'Auric babbler',
                'value' => 35
            ],
            'damascus_green_rifle' => [
                'title' => 'Damascus green rifle',
                'value' => 45
            ],
            'auric_harvester' => [
                'title' => 'Auric harvester',
                'value' => 55
            ],
            'chroma_welder' => [
                'title' => 'Chroma welder',
                'value' => 75
            ],
            'auric_bile_mine' => [
                'title' => 'Auric bile mine',
                'value' => 95
            ],
            'widow_skulk' => [
                'title' => 'Widow skulk',
                'value' => 150
            ],
            'chroma_exosuit' => [
                'title' => 'Chroma exosuit',
                'value' => 200
            ],
            'chroma_bmac' => [
                'title' => 'Chroma B.M.A.C.',
                'value' => 300
            ],
            'chroma_military_bmac' => [
                'title' => 'Chroma military B.M.A.C.',
                'value' => 500
            ],
            'auric_onos' => [
                'title' => 'Auric onos',
                'value' => 700
            ],
            'chroma_elite_assault_marine' => [
                'title' => 'Chroma elite assault marine',
                'value' => 1000
            ]
        ];
    }

    public function findTierBadges(): array
    {
        return [
            'tier1' => 2,
            'tier2' => 5,
            'tier3' => 20,
            'tier4' => 115,
            'tier5' => 200,
            'tier6' => 500,
            'tier7' => 1000,
            'tier8' => 1500
        ];
    }
}

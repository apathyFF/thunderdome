<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Pokedex API ("PokeAPI").
 *
 *  PokeAPI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PokeAPI is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PokeAPI.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Api\Controller;

use XF\Api\Controller\AbstractController;
use XF\Api\Mvc\Reply\ApiResult;
use XF\Mvc\ParameterBag;
use XF\Mvc\Entity\Entity;

class Player extends AbstractController
{
    protected function preDispatchController($action, ParameterBag $params)
    {
        $this->assertApiScopeByRequestMethod('hive');
    }

    public function actionGet(ParameterBag $params): ApiResult
    {
        if($params->steam_id)
        {
            $player = $this->assertViewablePlayer($params->steam_id);

            $result = [
                $player->name => $player->toApiResult(Entity::VERBOSITY_VERBOSE)
            ];

            return $this->apiResult($result);
        }

        $players = $this->finder('apathy\ThunderDome:Hive')->fetch();

        return $this->apiResult($players->toApiResults());
    }

    protected function assertViewablePlayer(int $id, $with = 'api'): Entity
    {
        return $this->assertViewableApiRecord('apathy\ThunderDome:Hive', $id, $with);
    }
}

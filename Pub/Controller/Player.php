<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Pub\Controller;

use apathy\ThunderDome\Entity\Hive;
use apathy\ThunderDome\Repository\Hive as RepositoryHive;
use XF;
use XF\Mvc\ParameterBag;
use XF\Mvc\Reply\Redirect;
use XF\Mvc\Reply\View;
use XF\Pub\Controller\AbstractController;

class Player extends AbstractController
{
    /** IDs of players who wish to remain excluded from the leaderboard */
    protected $excludedIds = [];

    public function actionIndex(ParameterBag $params)
    {
        if($params->steam_id)
        {
            return $this->rerouteController(__CLASS__, 'viewPlayer', $params);
        }

        /** @var \apathy\ThunderDome\ControllerPlugin\PlayerList $plugin */
        $plugin = $this->plugin('apathy\ThunderDome:PlayerList');
        $playerData = $plugin->getPlayerData();

        return $this->view('apathy\ThunderDome:Player', 'ap_td_player_list', $playerData);
    }

    public function actionFilters()
    {
        /** @var \apathy\ThunderDome\ControllerPlugin\PlayerList $plugin */
        $plugin = $this->plugin('apathy\ThunderDome:PlayerList');

        return $plugin->actionFilters();
    }

    public function actionViewPlayer(ParameterBag $params): View
    {
        $steamId = $params['steam_id'];
        $player = $this->assertViewablePlayer($steamId);

        $repo = $this->getHiveRepo();
        $history = $repo->findPlayerHistoryBySteamId($steamId, true);
        
        $stats = $this->drawSkillGraph($history);

        $page = $this->filterPage();
        $perPage = 10;

        $history->limitByPage($page, $perPage);

        $roundHistory = $history->order('date', 'desc')->fetch();

        $viewParams = [
            'history' => $roundHistory,
            'player'  => $player,
            'total'   => $history->total(),
            'page'    => $page,
            'perPage' => $perPage,
            'stats'   => $stats
        ];

        return $this->view('apathy\ThunderDome:Player', 'ap_td_player_view', $viewParams);
    }

    public function actionQuickRebuild(ParameterBag $params): Redirect
    {
        $steamId = $params['steam_id'];

        /** Return early since this player doesn't want a profile */
        if(isset($this->excludedIds[$steamId]))
        {
            return $this->redirect($this->getDynamicRedirect());
        }

        $repo = $this->getHiveRepo();
        $player = $repo->findPlayerBySteamId($steamId);

        if(!$player)
        {
            $player = $this->app()->em()->create('apathy\ThunderDome:Hive');
        }

        $fetch = $repo->fetchApi($steamId);

        /** This player likely has a private profile */
        if(!isset($fetch['playerstats']['stats']))
        {
            return $this->redirect($this->getDynamicRedirect());
        }

        $stats = $fetch['playerstats']['stats'];
        $stats = $this->formatPlayerStats($stats);

        /** 
         * Return early if they haven't played another game since their last check, 
         * or if the player simply does not exist
         */
        if(!$this->playerNeedsUpdating($player, $stats) || !isset($fetch['playerstats']))
        {
            return $this->redirect($this->getDynamicRedirect());
        }

        if(!empty($fetch['playerstats']['gameName']))
        {
            $stats['name'] = $fetch['playerstats']['gameName'];
        }
        else
        {
            $stats['name'] = $this->filter('name', 'str') ?: $player['name'];
        }

        $stats['steamID'] = $fetch['playerstats']['steamID'];

        $repo->rebuildPlayerData($player, $stats, XF::$time);

        return $this->redirect($this->getDynamicRedirect());
    }

    protected function formatPlayerStats(array $stats): array
    {
        /** Reindex the array according to the value of "name" */
        $stats = array_column($stats, null, 'name');

        /** Remove the "name" key */
        $stats = array_map(
            function($v) 
            {
                array_shift($v); 
        
                return $v;
            }, 
            $stats
        );

        return $stats;
    }

    protected function drawSkillGraph($entity): array
    {
        foreach($entity as $entry)
        {
            $date = isset($entry['date']) ? $entry['date'] : XF::$time;
            
            $skill  = $entry['td_skill'];
            $offset = $entry['td_skill_offset'];

            $commSkill  = $entry['td_comm_skill'];
            $commOffset = $entry['td_comm_skill_offset'];

            $marine[]     = ['x' => $date, 'y' => ( $skill + $offset )];
            $alien[]      = ['x' => $date, 'y' => ( $skill - $offset )];
            $marineComm[] = ['x' => $date, 'y' => ( $commSkill + $commOffset )];
            $alienComm[]  = ['x' => $date, 'y' => ( $commSkill - $commOffset )];

            $labels[] = date('Y-m-d', $date);
        }

        return [
            'marine'           => $marine     ?? [],
            'alien'            => $alien      ?? [],
            'marine_commander' => $marineComm ?? [],
            'alien_commander'  => $alienComm  ?? [],
            'labels'           => $labels     ?? []
        ];
    }

    public function actionFind(): View
    {
        $keywords = ltrim($this->filter('keywords', 'str', ['no-trim']));

        if($keywords !== '' && utf8_strlen($keywords) >= 2)
        {
            $repo = $this->getHiveRepo();

            $hive = $repo->findPlayersForList(true);
            $hive = $hive->where('name', 'LIKE', $hive->escapeLike($keywords, '?%'))->fetch(10);
        }
        else
        {
            $hive = [];
            $keywords = '';
        }

        $viewParams = [
            'keywords' => $keywords,
            'hive'     => $hive
        ];

        return $this->view('apathy\ThunderDome:Player\Find', '', $viewParams);
    }

    protected function playerNeedsUpdating(Hive $player, array $stats): bool
    {
        if($player->isInsert())
        {
            return true;
        }

        if(isset($stats['td_total_time_player']))
        {
            if($stats['td_total_time_player']['value'] != $player->td_total_time_player)
            {
                return true;
            }
        }

        if(isset($stats['td_total_time_commander']))
        {
            if($stats['td_total_time_commander']['value'] != $player->td_total_time_commander)
            {
                return true;
            }
        }

        if($stats['td_skill']['value'] != $player->td_skill
        || $stats['td_skill_offset']['value'] != $player->td_skill_offset)
        {
            return true;
        }

        if(isset($stats['td_comm_skill']))
        {
            if($stats['td_comm_skill']['value'] != $player->td_comm_skill
            || $stats['td_comm_skill_offset']['value'] != $player->td_comm_skill_offset)
            {
                return true;
            }
        }

        if($player->last_fetch_date === 0 
        && $player->last_fetch_date < strtotime('-5 minutes'))
        {
            return true;
        }

        return false;
    }

    protected function getHiveRepo(): RepositoryHive
    {
        return $this->repository('apathy\ThunderDome:Hive');
    }

    protected function assertViewablePlayer($steam_id, $with = NULL)
    {
        return $this->assertRecordExists('apathy\ThunderDome:Hive', $steam_id, $with, 'ap_td_no_player_found');
    }

    public static function getActivityDetails(array $activities): array
    {
        $app = XF::app();
        $router = $app->router('public');
        $defaultPhrase = $app->language()->phrase('ap_td_viewing_leaderboard');

        $output = [];

        foreach($activities AS $key => $activity)
        {
            $playerId = $activity['params'];

            $player = $app->em()->find('apathy\ThunderDome:Hive', $playerId);

            if(!empty($player))
            {
                $url = $router->buildLink('thunderdome', [
                    'name'     => $player->name,
                    'steam_id' => $player->steam_id
                ]);

                $output[$key] = [
                    'description' => $app->language()->phrase('ap_td_viewing_player_x'),
                    'title'       => $player->name,
                    'url'         => $url
                ];
            }
            else
            {
                $output[$key] = $defaultPhrase;
            }
        }

        return $output;
    }
}

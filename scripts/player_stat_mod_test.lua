Event.Hook("Console_api", function()
    local requestUrl = "https://fortreeforums.xyz/thunderdome/"
    local steamId    = tostring(Client.GetSteamId())

    local totalTimePlayer    = Client.GetUserStat_Int("td_total_time_player") or 0
    local totalTimeCommander = Client.GetUserStat_Int("td_total_time_commander") or 0
    local roundsWonPlayer    = Client.GetUserStat_Int("td_rounds_won_player") or 0
    local roundsWonCommander = Client.GetUserStat_Int("td_rounds_won_commander") or 0

    Shared.Message("Sending stats to " .. requestUrl .. steamId .. "/quick-rebuild")

    Shared.SendHTTPRequest(requestUrl .. steamId .. "/quick-rebuild", "GET", {
        td_total_time_player    = totalTimePlayer,
        td_total_time_commander = totalTimeCommander,
        td_rounds_won_player    = roundsWonPlayer,
        td_rounds_won_commander = roundsWonCommander
    })

    Shared.Message("Stats sent")
end)

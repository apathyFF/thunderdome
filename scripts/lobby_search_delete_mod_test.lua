Event.Hook("Console_api", function()

     -- Set up request data --
     local lobby   = Thunderdome():GetActiveLobby()
     local lobbyId = Thunderdome():GetActiveLobbyId()
     local steamId = tostring(Client.GetSteamId())

     local requestUrl = "https://lobby.fortreeforums.xyz/thunderdome/delete.php?id=" .. lobbyId .. "&steamId=" .. steamId

     -- Send the request --
     Shared.Message("Sending stats to " .. requestUrl)
     Shared.SendHTTPRequest(requestUrl, "GET")

end)

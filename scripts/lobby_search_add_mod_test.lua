Event.Hook("Console_api", function()

     -- Set up request data --
     local lobby   = Thunderdome():GetActiveLobby()
     local lobbyId = Thunderdome():GetActiveLobbyId()

     local requestUrl = "https://lobby.fortreeforums.xyz/thunderdome/add.php?id=" .. lobbyId

     player     = "&player[]="
     name       = "&name[]="
     skill      = "&skill[]="
     marine     = "&marine[]="
     alien      = "&alien[]="
     marineComm = "&marineComm[]="
     alienComm  = "&alienComm[]="

     generatedUrl = ""

     players = lobby:GetMembers()
     for i = 1, #players do
     local steamID64  = players[i]:GetField( LobbyMemberModelFields.SteamID64 )
     local playerName = players[i]:GetField( LobbyMemberModelFields.Name )

     local tdSkill      = players[i]:GetField( LobbyMemberModelFields.AvgSkill )        or 0
     local tdMarine     = players[i]:GetField( LobbyMemberModelFields.MarineSkill )     or 0
     local tdAlien      = players[i]:GetField( LobbyMemberModelFields.AlienSkill )      or 0
     local tdMarineComm = players[i]:GetField( LobbyMemberModelFields.MarineCommSkill ) or 0
     local tdAlienComm  = players[i]:GetField( LobbyMemberModelFields.AlienCommSkill )  or 0

     generatedUrl = player .. steamID64 .. name .. playerName .. skill.. tdSkill
     generatedUrl = generatedUrl .. marine .. tdMarine .. alien .. tdAlien
     generatedUrl = generatedUrl .. marineComm .. tdMarineComm .. alienComm .. tdAlienComm
     end

     local completeUrl = requestUrl .. generatedUrl

     -- Send the request --
     Shared.Message("Sending stats to " .. requestUrl)
     Shared.SendHTTPRequest(completeUrl, "GET")

end)

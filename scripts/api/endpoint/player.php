<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 */

namespace apathy\ThunderDome\scripts\api\endpoint;

require('/home/fortreef/public_html/src/XF.php');

use GuzzleHttp;
use XF;

class Response 
{
    protected static $app;

    public function __construct()
    {
        header('Content-Type: application/json; charset=utf-8');

        XF::start('/home/fortreef/public_html/');
        self::$app = XF::setupApp('XF\Pub\App');
    }

    public static function outputResponse()
    {
        $app = self::$app;
        $inputFilterer = $app->inputFilterer();

        if(isset($_GET['id']))
        {
            $steamId = $inputFilterer->filter($_GET['id'], 'uint');

            $player = $app->finder('apathy\ThunderDome:Hive')->where('steam_id', $steamId)->fetchOne();
            
            if(empty($player) || $player === NULL)
            {
                return print_r(GuzzleHttp\json_encode('player_not_found'));
            }

            $data['results'] = self::preparePlayerData($player);

            return print_r(GuzzleHttp\json_encode($data));
        }
        elseif(isset($_GET['name']))
        {
            $name = $inputFilterer->filter($_GET['name'], 'str');

            $player = $app->finder('apathy\ThunderDome:Hive')->where('name', $name)->fetchOne();

            if(empty($player)  || $player === NULL)
            {
                return print_r(GuzzleHttp\json_encode('player_not_found'));
            }

            $data['results'] = self::preparePlayerData($player);

            return print_r(GuzzleHttp\json_encode($data));
        }
        
        $finder = $app->finder('apathy\ThunderDome:Hive');

        $page = 1;
        $perPage = 10;
        $totalPlayers = $finder->total();
        
        if(isset($_GET['page']))
        {
            $page = $inputFilterer->filter($_GET['page'], 'uint');
        }

        $players = $finder->limitByPage($page, $perPage)->fetch();

        if(!empty($players))
        {
            $data['page'] = $page;
            $data['per_page'] = $perPage;
            $data['total_players'] = $totalPlayers;

            foreach($players as $player)
            {
                $data['results'][$player['name']] = self::preparePlayerData($player);
            }

            return print_r(GuzzleHttp\json_encode($data));
        }

        return print_r(GuzzleHttp\json_encode([]));
    }

    protected static function preparePlayerData($player): array
    {
        $totalLobbiesPlayed = $player->History->count();

        $player = $player->toArray();

        $fieldTime = floor($player['td_total_time_player'] / 3600);
        $fieldWins = $player['td_rounds_won_player'];

        $commTime = floor($player['td_total_time_commander'] / 3600);
        $commWins = $player['td_rounds_won_commander'];

        $data = $player;
        $data['total_lobbies_played'] = $totalLobbiesPlayed;
        $data['td_tier_badge'] = self::getTdTierBadge($fieldTime);
        $data['rewards'] = self::getRewards($fieldWins, $fieldTime, $commWins, $commTime);

        unset($data['player_id']);
    
        return $data;
    }

    protected static function getRewards(int $wins = 0, int $time = 0, int $commWins = 0, 
    int $commTime = 0): array
    {
        /** @var \apathy\ThunderDome\Repository\Reward $repo */
        $repo = self::$app->repository('apathy\ThunderDome:Reward');

        $tierBadges = $repo->findTierBadges();
        $playerTimeRewards = $repo->findTimeRewards();
        $playerWinRewards  = $repo->findWinRewards();

        $commTimeRewards = $repo->findCommTimeRewards();
        $commWinRewards  = $repo->findCommWinRewards();

        foreach($tierBadges as $codename => $value)
        {
            $codename = str_replace('tier', 'tier_', $codename);

            if($time >= $value)
            {
                $rewards['field']['time'][$codename] = true;
            }
            else
            {
                $rewards['field']['time'][$codename] = [
                    'unlocked' => false,
                    'progress' => ( $value - $time ),
                    'unit'     => 'hours'
                ];
            }
        }

        foreach($playerTimeRewards as $codename => $reward)
        {
            if($time >= $reward['value'])
            {
                $rewards['field']['time'][$codename] = true;
            }
            else
            {
                $rewards['field']['time'][$codename] = [
                    'unlocked' => false,
                    'progress' => ( $reward['value'] - $time ),
                    'unit'     => 'hours'
                ];
            }
        }

        foreach($playerWinRewards as $codename => $reward)
        {
            if($wins >= $reward['value'])
            {
                $rewards['field']['wins'][$codename] = true;
            }
            else
            {
                $rewards['field']['wins'][$codename] = [
                    'unlocked' => false,
                    'progress' => ( $reward['value'] - $wins ),
                    'unit'     => 'wins'
                ];
            }
        }

        foreach($commTimeRewards as $codename => $reward)
        {
            if($commTime >= $reward['value'])
            {
                $rewards['commander']['time'][$codename] = true;
            }
            else
            {
                $rewards['commander']['time'][$codename] = [
                    'unlocked' => false,
                    'progress' => ( $reward['value'] - $commTime ),
                    'unit'     => 'hours'
                ];
            }
        }

        foreach($commWinRewards as $codename => $reward)
        {
            if($commWins >= $reward['value'])
            {
                $rewards['commander']['wins'][$codename] = true;
            }
            else
            {
                $rewards['commander']['wins'][$codename] = [
                    'unlocked' => false,
                    'progress' => ( $reward['value'] - $commWins ),
                    'unit'     => 'wins'
                ];
            }
        }

        return $rewards;
    }

    protected static function getTdTierBadge(int $time): int
    {
        /** @var \apathy\ThunderDome\Repository\Reward $repo */
        $repo = self::$app->repository('apathy\ThunderDome:Reward');
        $tiers = $repo->findTierBadges();

        $playerTier = 0;

        foreach($tiers as $tier => $requirement)
        {
            if($time >= $requirement)
            {
                $playerTier = str_replace('tier', '', $tier);
            }
        }

        return intval($playerTier);
    }
}

$response = new Response();

$response->outputResponse();

?>
 

<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 */

namespace thunderdome;

class Lobby 
{
    protected $dir;

    protected $lobbyId;
    protected $steamIds;

    public function __construct()
    {
        header('Content-Type: application/json; charset=utf-8');

        $this->dir = __DIR__;

        $this->lobbyId = filter_input(INPUT_GET, 'lobbyId', FILTER_SANITIZE_NUMBER_INT);

        if(empty($this->lobbyId))
        {
            $this->lobbyId = rand();
        }

        for($i = 1; $i <= 12; $i++)
        {
            $this->steamIds[$i] = filter_input(INPUT_GET, 'player_' . $i, FILTER_SANITIZE_NUMBER_INT);
        }

        $this->steamIds = array_filter($this->steamIds, 'strlen');
    }

    public function createJsonForBot()
    {
        if(empty($this->steamIds))
        {
            return print_r(json_encode('no_player_ids_passed_to_endpoint'));
        }

        $filepath = $this->dir . '/lobbies/lobby_' . $this->lobbyId . '.json';
        $steamIds = $this->convertToSteamId3();

        file_put_contents($filepath, $steamIds, LOCK_EX);

        return print_r(json_encode('success'));
    }

    protected function convertToSteamId3()
    {
        foreach($this->steamIds as $player => $steamId)
        {
            $steamId = intval($steamId);

            if($steamId === NULL || $steamId === 0)
            {
                continue;
            }

            /** Convert to Steam Id3 */
            $players[$player] = ( $steamId - 76561197960265728 );
        }

        return $players;
    }
}

$lobby = new Lobby();

$lobby->createJsonForBot();

?>
 

<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */ 

require ('./src/XF.php');

XF::start(__DIR__);
$app = XF::setupApp('XF\Pub\App');
$app->start();

$fetchDate = 1676729790;

$players = $app->finder('apathy\ThunderDome:Hive')
               ->where('last_fetch_date', '<', 1676729790)
               ->fetch();

foreach($players as $player)
{
    $app->http()->client()->get('https://fortreeforums.xyz/thunderdome/' . $player->steam_id . '/quick-rebuild?name=' . $player->name);
}

?>
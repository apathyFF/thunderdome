<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Widget;

use XF;
use XF\Widget\AbstractWidget;
use XF\Widget\WidgetRenderer;

class HighestCommPlaytime extends AbstractWidget
{
    public function render(): WidgetRenderer
    {
        $finder = $this->finder('apathy\ThunderDome:Hive');

        $player = $finder->where('td_total_time_commander', '>=', 1)
                         ->order('td_total_time_commander', 'desc')
                         ->fetch(1)
                         ->first();

        $viewParams = [
            'player' => $player,
            'title'  => $this->getTitle() ?: XF::phrase('ap_td_highest_comm_playtime')
        ];

        return $this->renderer('ap_td_comm_playtime_widget', $viewParams);
    }

    public function getOptionsTemplate()
    {
        return '';
    }
}

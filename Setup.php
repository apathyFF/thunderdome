<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $this->schemaManager()->createTable('xf_ap_td_hive', function(Create $table)
        {
            $table->addColumn('player_id', 'int')->autoIncrement();
            $table->addPrimaryKey('player_id');
            $table->addColumn('steam_id', 'int')->nullable()->setDefault(0);
            $table->addColumn('name', 'varchar', 50);
            $table->addColumn('td_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_alien_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_marine_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_skill_offset', 'int')->unsigned(false)->nullable()->setDefault(0);
            $table->addColumn('td_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_alien_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_marine_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_comm_skill_offset', 'int')->unsigned(false)->nullable()->setDefault(0);
            $table->addColumn('tier', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_total_time_player', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_total_time_commander', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_rounds_won_player', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_rounds_won_commander', 'int')->nullable()->setDefault(0);
            $table->addColumn('last_fetch_date', 'int')->nullable()->setDefault(0);
        });
    }

    public function uninstallStep1()
    {
        $this->schemaManager()->dropTable('xf_ap_td_hive');
    }

    public function installStep2()
    {
        $this->schemaManager()->createTable('xf_ap_td_history', function(Create $table)
        {
            $table->addColumn('entry_id', 'int')->autoIncrement();
            $table->addPrimaryKey('entry_id');
            $table->addColumn('steam_id', 'int')->nullable()->setDefault(0);
            $table->addColumn('name', 'varchar', 50);
            $table->addColumn('td_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_alien_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_marine_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_skill_offset', 'int')->unsigned(false)->nullable()->setDefault(0);
            $table->addColumn('td_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_alien_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_marine_comm_skill', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_comm_skill_offset', 'int')->unsigned(false)->nullable()->setDefault(0);
            $table->addColumn('tier', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_total_time_player', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_total_time_commander', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_rounds_won_player', 'int')->nullable()->setDefault(0);
            $table->addColumn('td_rounds_won_commander', 'int')->nullable()->setDefault(0);
            $table->addColumn('date', 'int')->nullable()->setDefault(0);
        });
    }

    public function uninstallStep2()
    {
        $this->schemaManager()->dropTable('xf_ap_td_history');
    }
}

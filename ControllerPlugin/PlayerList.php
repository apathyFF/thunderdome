<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\ControllerPlugin;

use apathy\ThunderDome\Finder\Hive as Player;
use XF\ControllerPlugin\AbstractPlugin;

class PlayerList extends AbstractPlugin
{
    public function getPlayerData(): array
    {
        /** @var \apathy\ThunderDome\Repository\Hive $repo */
        $repo = $this->repository('apathy\ThunderDome:Hive');

        $finder = $repo->findPlayersForList(true);
        $finder = $finder->setDefaultOrder('td_skill', 'desc');

        $filters = $this->getPlayerFilterInput();

        $this->applyPlayerFilters($finder, $filters);

        $page = $this->filterPage();
        $perPage = 15;

        $finder->limitByPage($page, $perPage);

        return [
            'players' => $finder->fetch()->filterViewable(),
            'filters' => $filters,
            'total'   => $finder->total(),
            'page'    => $page,
            'perPage' => $perPage
        ];
    }

    public function applyPlayerFilters(Player $player, array $filters)
    {
        if(!empty($filters['tier']))
        {
            $tiers = [];

            foreach($filters['tier'] as $tier)
            {
                $tiers[] = ['tier', $tier];
            }

            $player->whereOr($tiers);
        }

        if(!empty($filters['has_commanded']))
        {
            $player->hasCommanded();
        }

        if(!empty($filters['order']))
        {
            $player->order($filters['order'], $filters['direction']);
        }
    }

    public function getPlayerFilterInput(): array
    {
        $filters = [];

        $input = $this->filter([
            'tier'          => 'array',
            'has_commanded' => 'bool',
            'order'         => 'str',
            'direction'     => 'str'
        ]);

        if($input['tier'])
        {
            $filters['tier'] = $input['tier'];
        }

        if($input['has_commanded'])
        {
            $filters['has_commanded'] = $input['has_commanded'];
        }

        if(!empty($input['order']) && !empty($input['direction']))
        {
            $filters['order'] = $input['order'];
            $filters['direction'] = $input['direction'];
        }

        return $filters;
    }

    public function actionFilters()
    {
        $filters = $this->getPlayerFilterInput();

        if($this->filter('apply', 'bool'))
        {
            return $this->redirect($this->buildLink('thunderdome', null, $filters));
        }

        if(empty($filters['order']))
        {
            $filters['order'] = 'td_skill';
        }
        if(empty($filters['direction']))
        {
            $filters['direction'] = 'desc';
        }

        $viewParams = [
            'filters' => $filters
        ];

        return $this->view('apathy\ThunderDome:Player', 'ap_td_player_list_filters', $viewParams);
    }
}


<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Entity;

use XF;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 * @property int|null $entry_id
 * @property string   $name
 * @property int      $steam_id
 * @property int      $td_skill
 * @property int      $td_alien_skill
 * @property int      $td_marine_skill
 * @property int      $td_skill_offset
 * @property int      $td_comm_skill
 * @property int      $td_alien_comm_skill
 * @property int      $td_marine_comm_skill
 * @property int      $td_comm_skill_offset
 * @property int      $tier
 * @property int      $td_total_time_player
 * @property int      $td_total_time_commander
 * @property int      $td_rounds_won_player
 * @property int      $td_rounds_won_commander
 * @property int      $date
 * 
 * RELATIONS
 * @property \apathy\ThunderDome\Entity\Hive $Hive
 */

class History extends Entity
{
    public static function getStructure(Structure $structure): Structure
    {
        $structure->table = 'xf_ap_td_history';
        $structure->shortName = 'apathy\ThunderDome:History';
        $structure->primaryKey = 'entry_id';
        $structure->columns = [
            'entry_id' => [
                'type'          => self::UINT,
                'default'       => NULL,
                'autoIncrement' => true
            ],
            'name' => [
                'type'          => self::STR,
                'maxLength'     => 50,
                'default'       => false
            ],
            'steam_id' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_alien_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_marine_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_skill_offset' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_alien_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_marine_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_comm_skill_offset' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'tier' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_total_time_player' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_total_time_commander' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_rounds_won_player' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_rounds_won_commander' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'date' => [
                'type'          => self::UINT,
                'default'       => XF::$time
            ]
        ];
        $structure->getters   = [];
        $structure->relations = [
            'Hive' => [
                'entity'     => 'apathy\ThunderDome:Hive',
                'type'       => self::TO_ONE,
                'conditions' => 'steam_id',
                'primary'    => true
            ]
        ];

        return $structure;
    }
}

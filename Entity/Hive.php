<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] ThunderDome ("ThunderDome").
 *
 *  ThunderDome is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ThunderDome is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ThunderDome.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ThunderDome\Entity;

use XF;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 * @property int    $player_id
 * @property string $name
 * @property int    $steam_id
 * @property int    $td_skill
 * @property int    $td_alien_skill
 * @property int    $td_marine_skill
 * @property int    $td_skill_offset
 * @property int    $td_comm_skill
 * @property int    $td_alien_comm_skill
 * @property int    $td_marine_comm_skill
 * @property int    $td_comm_skill_offset
 * @property int    $tier
 * @property int    $td_total_time_player
 * @property int    $td_total_time_commander
 * @property int    $td_rounds_won_player
 * @property int    $td_rounds_won_commander
 * @property int    $last_fetch_date
 * 
 * GETTERS
 * @property int    $alien_skill
 * @property int    $marine_skill
 * @property int    $alien_comm_skill
 * @property int    $marine_comm_skill
 * @property int    $lobbies_played
 * 
 * RELATIONS
 * @property \apathy\ThunderDome\Entity\History $History
 */

class Hive extends Entity
{
    public function findTierBadges(): array
    {
        /** @var \apathy\ThunderDome\Repository\Reward $repo */
        $repo = $this->repository('apathy\ThunderDome:Reward');

        $time = floor(( $this->td_total_time_player / 3600 ));

        $badges = $repo->findTierBadges();

        $locked = [];
        $unlocked = [];

        foreach($badges as $tier => $requirement)
        {
            if($time >= $requirement)
            {
                $unlocked[] = [
                    'codename' => $tier,
                    'title'    => ucfirst(str_replace('tier', 'tier ', $tier))
                ];
            }
            else
            {
                $locked[] = [
                    'codename'       => $tier,
                    'title'          => ucfirst(str_replace('tier', 'tier ', $tier)),
                    'requirement'    => $requirement,
                    'unlock_in'      => ( $requirement - $time ),
                    'unlock_percent' => round(( ( $time / $requirement ) * 100 ))
                ];
            }
        }

        return ['locked' => $locked, 'unlocked' => $unlocked];
    }

    public function findRewards(string $type): array
    {
        /** @var \apathy\ThunderDome\Repository\Reward $repo */
        $repo = $this->repository('apathy\ThunderDome:Reward');

        switch($type)
        {
            case 'time:player':
                $value = floor($this->td_total_time_player / 3600);
                $rewards = $repo->findTimeRewards();
            break;
            case 'time:commander':
                $value = floor($this->td_total_time_commander / 3600);
                $rewards = $repo->findCommTimeRewards();
            break;
            case 'win:player':
                $value = $this->td_rounds_won_player;
                $rewards = $repo->findWinRewards();
            break;
            case 'win:commander':
                $value = $this->td_rounds_won_commander;
                $rewards = $repo->findCommWinRewards();
            break;
        }

        $locked   = [];
        $unlocked = [];

        foreach($rewards as $codename => $requirement)
        {
            if($value <= $requirement['value'])
            {
                $locked[] = [
                    'codename'       => $codename,
                    'title'          => $requirement['title'],
                    'requirement'    => $requirement['value'],
                    'unlock_in'      => ( $requirement['value'] - $value ),
                    'unlock_percent' => round(( ( $value / $requirement['value'] ) * 100 ))
                ];
            }
            else
            {
                $unlocked[] = [
                    'codename'    => $codename,
                    'title'       => $requirement['title'],
                    'requirement' => $requirement['value']
                ];
            }
        }

        return ['locked' => $locked, 'unlocked' => $unlocked];
    }

    public function findTier(int $skill = NULL): int
    {
        if($skill === NULL)
        {
            $skill = $this->td_skill;
        }

        switch(true)
        {
            case ( $skill >= 4101 ): return 7;
            case ( $skill >= 2901 ): return 6;
            case ( $skill >= 2101 ): return 5;
            case ( $skill >= 1401 ): return 4;
            case ( $skill >= 751 ):  return 3;
            case ( $skill >= 301 ):  return 2;
            case ( $skill <= 300 ):  return 1;
        }
    }

    public function findTime(string $type)
    {
        if($type == 'player')
        {
            $time = $this->td_total_time_player;
        }
        else
        {
            $time = $this->td_total_time_commander;
        }

        $util = new XF\Util\Time;

        return $util::getIntervalArray($time, true);

    }

    public function getAlienSkill(): int
    {
        if(empty($this->td_alien_skill))
        {
            return ( $this->td_skill - $this->td_skill_offset );
        }

        return $this->td_alien_skill;
    }

    public function getAlienCommSkill(): int
    {
        if(empty($this->td_alien_comm_skill))
        {
            return ( $this->td_comm_skill - $this->td_comm_skill_offset );
        }

        return $this->td_alien_comm_skill;
    }

    public function getMarineSkill(): int
    {
        if(empty($this->td_marine_skill))
        {
            return ( $this->td_skill + $this->td_skill_offset );
        }

        return $this->td_marine_skill;
    }

    public function getMarineCommSkill(): int
    {
        if(empty($this->td_marine_comm_skill))
        {
            return ( $this->td_comm_skill + $this->td_comm_skill_offset );
        }

        return $this->td_marine_comm_skill;
    }

    public function getLobbiesPlayed(): int
    {
        if($this->getRelation('History'))
        {
            return $this->History->count();
        }

        return 0;
    }

    protected function _postSave()
    {
        if($this->isInsert())
        {
            $this->app()->jobManager()->enqueueUnique(
                'SearchRebuild',
                'XF:SearchRebuild',
                ['rebuild_types' => 'td_player'],
                false
            );
        }
    }

    public static function getStructure(Structure $structure): Structure
    {
        $structure->table = 'xf_ap_td_hive';
        $structure->shortName = 'apathy\ThunderDome:Hive';
        $structure->primaryKey = 'steam_id';
        $structure->columns = [
            'player_id' => [
                'type'          => self::UINT,
                'autoIncrement' => true
            ],
            'name' => [
                'type'          => self::STR,
                'maxLength'     => 50,
                'default'       => false
            ],
            'steam_id' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_alien_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_marine_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_skill_offset' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_alien_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_marine_comm_skill' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'td_comm_skill_offset' => [
                'type'          => self::INT,
                'default'       => 0
            ],
            'tier' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_total_time_player' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_total_time_commander' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_rounds_won_player' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'td_rounds_won_commander' => [
                'type'          => self::UINT,
                'default'       => 0
            ],
            'last_fetch_date' => [
                'type'          => self::UINT,
                'default'       => XF::$time
            ]
        ];
        $structure->getters = [
            'alien_skill'       => true,
            'marine_skill'      => true,
            'alien_comm_skill'  => true,
            'marine_comm_skill' => true,
            'lobbies_played'    => true
        ];
        $structure->relations = [
            'History' => [
                'entity'     => 'apathy\ThunderDome:History',
                'type'       => self::TO_MANY,
                'conditions' => 'steam_id',
                'primary'    => true
            ]
        ];

        return $structure;
    }

    public function canView(&$error = null)
    {
        return true;
    }
}
